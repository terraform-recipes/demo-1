resource "aws_key_pair" "ecsdemo" {
  key_name = "${var.app}-keypair"
  public_key = "${file("${var.public_key}")}"
}

resource "aws_launch_configuration" "ecs" {
    name = "ECS ${var.app}-ecs-cluster"
    image_id = "${lookup(var.ami, var.region)}"
    instance_type = "${var.instance_type}"
    iam_instance_profile = "${aws_iam_instance_profile.ecsdemo.name}"
    key_name = "${aws_key_pair.ecsdemo.key_name}"
    security_groups = ["${aws_security_group.allow_all.id}"]
    user_data = "#!/bin/bash\necho ECS_CLUSTER=${var.app}-ecs-cluster > /etc/ecs/ecs.config"
}
