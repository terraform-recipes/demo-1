resource "aws_lb" "lb" {
  name               = "${var.app}-ecsdemo-lb"
  internal           = false
  load_balancer_type = "network"
  subnets            = ["${aws_subnet.subnet-1.id}","${aws_subnet.subnet-2.id}"]
  #  security_groups    = ["${aws_security_group.allow_all.id}"]

  tags = {
    Environment = "development"
  }
}

resource "aws_lb_target_group" "lb-tg" {
  name     = "${var.app}-ecsdemo-lb-tg"
  port     = 80
  protocol = "TCP"
  vpc_id   = "${aws_vpc.vpc.id}"
  depends_on = [
        "aws_lb.lb"
  ]
}

resource "aws_lb_listener" "lb-listener" {
  load_balancer_arn = "${aws_lb.lb.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.lb-tg.id}"
    type = "forward"
  }
}
