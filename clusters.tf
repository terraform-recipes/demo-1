resource "aws_ecs_cluster" "ecsdemo" {
  name = "${var.app}-ecs-cluster"
}

resource "aws_ecs_service" "ecsdemo" {
  name            = "${var.app}-ecs-service"
  cluster         = "${aws_ecs_cluster.ecsdemo.id}"
  task_definition = "${aws_ecs_task_definition.ecsdemo.arn}"
  desired_count   = "${var.desired_capacity}"
}
