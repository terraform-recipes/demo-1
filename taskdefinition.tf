resource "aws_ecs_task_definition" "ecsdemo" {
  family                = "${var.app}-taskdefinition"
  container_definitions = "${file("${var.taskdefinition}")}"

  volume {
    name      = "db_data"
    host_path = "${lookup(var.path, "db_data")}"
  }

  # Named Volume
  volume {
    name      = "app"
    host_path = "${lookup(var.path, "app")}"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${join(",", local.zones)}]"
  }
}
