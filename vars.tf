variable access_key {}
variable secret_key {}

variable public_key {}
variable private_key {}

variable app { default = "ecsdemo" }

variable instance_type {
  default = "t2.micro"
}

variable region {
  default = "us-west-2"
}

variable ami {
  type = "map"
  default = {
    "us-west-2" = "ami-00430184c7bb49914"
    "us-west-1" = "ami-0e7dd5fe55b87a5fe"
  }
}

variable taskdefinition { default = "task-definition.json" }

variable path {
  type = "map"
  default = {
    db_data = "/data/mysql"
    app = "/var/www/html"
  }
}
variable security_group_id { default = ["sg-f5cfdc84"] }
variable min_size { default = 1 }
variable max_size { default = 2 }
variable desired_capacity { default = 1 }
variable health_check_grace_period { default = 300 }

variable iam_instance_profile { default = "" }
locals {
  zones = ["${var.region}a", "${var.region}b"]
}
