resource "aws_autoscaling_group" "ecs-cluster" {
  vpc_zone_identifier = ["${aws_subnet.subnet-1.id}, ${aws_subnet.subnet-2.id}"]
  name = "ECS ${var.app}-ecs-cluster"
  min_size = "${var.min_size}"
  max_size = "${var.max_size}"
  desired_capacity = "${var.desired_capacity}"
  health_check_type = "EC2"
  launch_configuration = "${aws_launch_configuration.ecs.name}"
  health_check_grace_period = "${var.health_check_grace_period}"
  target_group_arns = ["${aws_lb_target_group.lb-tg.arn}"]

  depends_on = [
    "aws_lb_target_group.lb-tg"
  ]

  tag {
    key = "Env"
    value = "${var.app}-ecs-cluster"
    propagate_at_launch = true
  }

  tag {
    key = "Name"
    value =  "ECS ${var.app}-ecs-cluster"
    propagate_at_launch = true
  }
}
